import json
import os
import re

__all__ = []


class MockDisplay:
    def debug(self, *_args, **kwargs):
        pass

    def warning(self, *_args, **kwargs):
        pass

    def v(self, *_args, **kwargs):
        pass

    # pylint: disable=invalid-name
    def vv(self, *_args, **kwargs):
        pass

    def vvv(self, *_args, **kwargs):
        pass

    def vvvv(self, *_args, **kwargs):
        pass


def assets(subdir, filename):
    """
    Take all files from subdir, relative to this file, that
    match regex filename and return them as a list

    :params subdir: Directory to search
    :params filename: Regex to match
    :returns: List of matching files
    """
    prog = re.compile(filename)

    path = os.path.dirname(os.path.realpath(__file__))
    path = os.path.join(path, subdir)

    files = os.listdir(path)
    files = [os.path.join(path, f) for f in files if re.match(prog, f)]
    files.sort()
    return files


def get_expected_results(src):
    src = os.path.splitext(src)[0] + ".json"
    if os.path.exists(src):
        with open(src, encoding="UTF-8") as fd:
            return json.load(fd)

    src = os.path.splitext(src)[0] + ".txt"
    if os.path.exists(src):
        with open(src, encoding="UTF-8") as fd:
            return fd.read()

    return None
