Ansible roster inventory plugin
===============================

`Roster` is an `Ansible` inventory plugin with focus on groups applied to hosts instead of hosts included in groups. It supports ranges (eg: "[0:9]"), regex hostnames (eg: "(dev|prd)-srv"), file inclusions, and variable merging.

.. glossary::

   Roster
     A list of members of a team or organization, in particular of sports players available for team selection.

.. toctree::
   :maxdepth: 2
   :caption: Contents

   introduction
   roster
   hosts
   groups
   vars
   examples

.. toctree::
   :maxdepth: 2
   :caption: Miscellaneous

   license

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
