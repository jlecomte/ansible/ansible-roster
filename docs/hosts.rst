
Hosts
=====

A `hosts` key denotes the definition of hosts. At least one host must be present for the inventory to be considered valid.

Multiple `hosts` sections are allowed throughout the inventory with the yaml limitation of one per file. For example, you can have a folder with one file per host, and include the whole folder with the '*' wildcard.

A host is defined by it's name and can contain group memberships and variables.

Any redeclaration of a host will mask the previous declaration of the host.

.. code-block:: yaml

  ---
  plugin: roster

  hosts:
    # the use of a FQDN here is purely arbitrary
    one.example.com:
      groups:
        - group_one
        - group_two
      vars:
        foobar1: "one"
        foobar2:
          - name: "Example"
            state: "present"


Keys
----

A `hosts` section supports the following keys:

* `groups`
* `vars`


Range expansion
---------------

Ranges are specified by a integer lower and upper bound in square braces, separated by a colon.

All expanded hostnames will contain the same groups and variables as the source.

For example, if you have `example1`, `example2`, `example3`, and `example4` being 4 identical machines, you can specify them as a single host:

.. code-block:: yaml

  ---
  plugin: roster

  hosts:
    # Expand into 4 hosts named example1 to example4
    example[1:4]:
      groups:
        - group_one
      vars:
        foobar: "one"


Regex expansion
---------------

Hosts can be declared with regex that will be expanded into all matching strings to a given regular expression.

A host name using regex must be enclosed in parentheses, such as `(one|two)bar`. The latter would be expanded into `onebar` and `twobar`.

  .. warning:: A hard limit of 1000 expanded hosts exists. If the regex would expand to over 1000 lines then the plugin will fail with an error message.

.. code-block:: yaml

  ---
  plugin: roster

  hosts:
    # Generate 2 hosts: frontend.example.com and backend.example.com
    (front|back)end.example.com:
      groups:
        - group_one
      vars:
        foobar: "one"

    # Generate 26 hosts
    foobar-([a-z]).example.com:
      vars:
        foobar: "one"
